//
//  Copyright © 2017 fdapps. All rights reserved.
//

import UIKit

extension PanelController: UIGestureRecognizerDelegate {
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        switch gestureRecognizer {
        case panGesture:
            
            return state == PanelState.OPENED
        case screenEdgePanGesture:
            
            return state == PanelState.CLOSED
        default:
            
            return touch.view == gestureRecognizer.view
        }
    }
}
