//
//  Copyright © 2017 fdapps. All rights reserved.
//

import UIKit

@objc public class PanelController: UIViewController {
    
    //Mark: properties
    
    private let maxAlpha = 0.2 as CGFloat
    private let animateDuration = 0.25 as TimeInterval
    private var containerConstraint: NSLayoutConstraint?
    private var widthConstraint: NSLayoutConstraint?
    private var startLocation = CGPoint.zero
    private var delta = 0.0 as CGFloat
    private let shadowColor = UIColor.black
    private let shadowOpacity = 0.4 as Float
    private var shadowRadius = 5.0 as CGFloat
    
    @IBInspectable public var firstIdentifier: String?
    @IBInspectable public var secondIdentifier: String?
    @IBInspectable public var width = 280 as CGFloat {
        
        didSet {
            
            if let widthConstraint = widthConstraint {
                
                widthConstraint.constant = width
            }
        }
    }
    
    lazy private var containerView: UIView = {
        
        let view = UIView(frame: self.view.frame)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(white: 0.0, alpha: 0)
        
        let tapGesture = UITapGestureRecognizer(
            target: self,
            action: #selector(PanelController.containerViewTapped(gesture:))
        )
        
        view.addGestureRecognizer(tapGesture)
        tapGesture.delegate = self
        
        return view
        
    }()
    
    lazy public private(set) var screenEdgePanGesture: UIScreenEdgePanGestureRecognizer = {
        
        let gesture = UIScreenEdgePanGestureRecognizer(
            target: self,
            action: #selector(PanelController.handlePanGesture(gesture:))
        )
        
        if self.direction == PanelDirection.LEFT {
            
            gesture.edges = .left
        } else {
            
            gesture.edges = .right
        }
        
        gesture.delegate = self
        return gesture
    }()
    
    lazy public private(set) var panGesture: UIPanGestureRecognizer = {
        
        let gesture = UIPanGestureRecognizer(
            target: self,
            action: #selector(PanelController.handlePanGesture(gesture:))
        )
        
        gesture.delegate = self
        return gesture
    }()
    
    public var delegate: PanelControllerDelegate?
    
    public var direction = PanelDirection.LEFT {
        
        didSet {
            
            if direction == PanelDirection.LEFT {
                
                screenEdgePanGesture.edges = .left
            } else {
                
                screenEdgePanGesture.edges = .right
            }
            
            let tmp = secondController
            secondController = tmp
        }
    }
    
    public var state: Int {
        get {

            return containerView.isHidden ? PanelState.CLOSED : PanelState.OPENED
        }
        set {
            
            set(state: state, animated: false)
        }
    }
    
    public var firstController: UIViewController? {
        
        didSet {

            if let oldController = oldValue {
                
                oldController.willMove(toParentViewController: nil)
                oldController.view.removeFromSuperview()
                oldController.removeFromParentViewController()
            }
            
            if let controller = firstController {
                
                addChildViewController(controller)
                view.insertSubview(controller.view, at: 0)
                
                controller.view.translatesAutoresizingMaskIntoConstraints = false
                
                let views = ["first": controller.view]
                
                view.addConstraints(
                    NSLayoutConstraint.constraints(
                        withVisualFormat: "V:|-0-[first]-0-|",
                        options: [],
                        metrics: nil,
                        views: views
                    )
                )
                view.addConstraints(
                    NSLayoutConstraint.constraints(
                        withVisualFormat: "H:|-0-[first]-0-|",
                        options: [],
                        metrics: nil,
                        views: views
                    )
                )
                
                controller.didMove(toParentViewController: self)
            }
        }
    }
    
    public var secondController: UIViewController? {
        
        didSet {
            
            if let oldController = oldValue {
                
                oldController.willMove(toParentViewController: nil)
                oldController.view.removeFromSuperview()
                oldController.removeFromParentViewController()
            }
            
            if let controller = secondController {
                
                controller.view.layer.shadowColor = shadowColor.cgColor
                controller.view.layer.shadowOpacity = shadowOpacity
                controller.view.layer.shadowRadius  = shadowRadius
                
                addChildViewController(controller)
                containerView.addSubview(controller.view)
                
                controller.view.translatesAutoresizingMaskIntoConstraints = false
                
                let views = ["second": controller.view]
                
                widthConstraint = NSLayoutConstraint(
                    item: controller.view,
                    attribute: NSLayoutAttribute.width,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: nil,
                    attribute: NSLayoutAttribute.width,
                    multiplier: 1,
                    constant: width
                )
                
                if let constraint = widthConstraint {
                    
                    controller.view.addConstraint(constraint)
                }
                
                var itemAttribute: NSLayoutAttribute? = nil
                var toItemAttribute: NSLayoutAttribute? = nil
                
                if direction == PanelDirection.LEFT {
                    
                    itemAttribute   = .right
                    toItemAttribute = .left
                } else {
                    
                    itemAttribute   = .left
                    toItemAttribute = .right
                }
                
                if let itemAttribute = itemAttribute, let toItemAttribute = toItemAttribute {
                    
                    containerConstraint = NSLayoutConstraint(
                        item: controller.view,
                        attribute: itemAttribute,
                        relatedBy: NSLayoutRelation.equal,
                        toItem: containerView,
                        attribute: toItemAttribute,
                        multiplier: 1,
                        constant: 0
                    )
                    
                    if let constraint = containerConstraint {
                        
                        containerView.addConstraint(constraint)
                        containerView.addConstraints(
                            NSLayoutConstraint.constraints(
                                withVisualFormat: "V:|-0-[second]-0-|",
                                options: [],
                                metrics: nil,
                                views: views
                            )
                        )
                    }
                }
                
                containerView.updateConstraints()
                controller.updateViewConstraints()
                controller.didMove(toParentViewController: self)
            }
        }
    }
    
    //Mark: delegate
    
    @objc dynamic public func containerViewTapped(gesture: UITapGestureRecognizer) {
        
        set(state: PanelState.CLOSED, animated: true)
    }
    
    @objc dynamic public func handlePanGesture(gesture: UIGestureRecognizer) {
        
        containerView.isHidden = false
        
        if gesture.state == .began {
            
            startLocation = gesture.location(in: view)
        }
        
        let delta = CGFloat(gesture.location(in: view).x - startLocation.x)
        var constant: CGFloat
        var backGroundAlpha: CGFloat
        var state: Int
        
        if direction == PanelDirection.LEFT {
            
            state = self.delta < 0 ? PanelState.CLOSED : PanelState.OPENED
            
            constant = min(containerConstraint!.constant + delta, width)
            
            backGroundAlpha = min(
                maxAlpha,
                maxAlpha * (abs(constant)/width)
            )
        } else {
            
            state = self.delta > 0 ? PanelState.CLOSED : PanelState.OPENED
            
            constant = max(containerConstraint!.constant + delta, -width)
            
            backGroundAlpha = min(
                maxAlpha,
                maxAlpha * (abs(constant)/width)
            )
        }
        
        containerConstraint?.constant = constant
        containerView.backgroundColor = UIColor(
            white: 0,
            alpha: backGroundAlpha
        )
        
        switch gesture.state {
        case .changed:
            
            startLocation = gesture.location(in: view)
            self.delta = delta
            break
        case .ended, .cancelled:
            
            set(state: state, animated: true)
            break
        default:
            break
        }
    }
    
    //Mark: methods
    
    public convenience init(direction: Int, width: CGFloat) {
        self.init()
        
        self.direction = direction
        self.width = width
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addGestureRecognizer(screenEdgePanGesture)
        view.addGestureRecognizer(panGesture)
        
        view.addSubview(containerView)
        
        let views = ["containerView": containerView]
        
        view.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "H:|-0-[containerView]-0-|",
                options: [],
                metrics: nil,
                views: views
            )
        )
        
        view.addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: "V:|-0-[containerView]-0-|",
                options: [],
                metrics: nil,
                views: views
            )
        )
        
        containerView.isHidden = true
        
        if let identifier = firstIdentifier {
            
            performSegue(withIdentifier: identifier, sender: self)
        }
        
        if let identifier = secondIdentifier {
            
            performSegue(withIdentifier: identifier, sender: self)
        }
    }
    
    public func set(state: Int, animated: Bool) {
        
        containerView.isHidden = false
        
        let duration = animated ? animateDuration : 0 as TimeInterval
        
        UIView.animate(withDuration: duration,
                       delay: 0,
                       options: UIViewAnimationOptions.curveLinear,
                       animations: { () -> Void in
                        if state == PanelState.CLOSED {
                            
                            self.containerConstraint?.constant = 0
                            
                            self.containerView.backgroundColor = UIColor(white: 0, alpha: 0)
                        } else {
                            
                            if self.direction == PanelDirection.LEFT {
                                
                                self.containerConstraint?.constant = self.width
                                
                            } else {
                                
                                self.containerConstraint?.constant = -self.width
                            }
                            
                            self.containerView.backgroundColor = UIColor(white: 0, alpha: self.maxAlpha)
                        }
                        
                        self.containerView.layoutIfNeeded()
                        
        }) { (finished: Bool) -> Void in
            
            if finished {
                
                self.containerView.isHidden = state == PanelState.CLOSED
            
                if let delegate = self.delegate {
                    
                    if let panelChanged = delegate.panelChanged {
                        
                        panelChanged(self, state)
                    }
                }
            }
        }
    }
}

