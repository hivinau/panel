//
//  Copyright © 2017 fdapps. All rights reserved.
//

@objc public protocol PanelControllerDelegate: class {
    
    @objc optional func panelChanged(panelController: PanelController, state: Int)
}
