//
//  Copyright © 2017 fdapps. All rights reserved.
//

import UIKit

public class PanelControllerFirstSegue: UIStoryboardSegue {
    
    final override public func perform() {
        super.perform()
        
        if let controller = source as? PanelController {
            
            controller.firstController = destination
            
        } else {
            
            assertionFailure("source controller must be PanelController !")
        }
    }
}
