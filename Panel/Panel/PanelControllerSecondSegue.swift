//
//  Copyright © 2017 fdapps. All rights reserved.
//

import UIKit

public class PanelControllerSecondSegue: UIStoryboardSegue {
    
    final override public func perform() {
        super.perform()
        
        if let controller = source as? PanelController {
            
            controller.secondController = destination
            
        } else {
            
            assertionFailure("source controller must be PanelController !")
        }
    }
}
