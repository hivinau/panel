//
//  Copyright © 2017 fdapps. All rights reserved.
//

public extension PanelController {
    
    @objc public class PanelDirection: NSObject {
        
        public static let LEFT = 0
        public static let RIGHT = 1
    }
}
