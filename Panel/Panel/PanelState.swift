//
//  Copyright © 2017 fdapps. All rights reserved.
//

public extension PanelController {
    
    @objc public class PanelState: NSObject {
        
        public static let OPENED = 0
        public static let CLOSED = 1
    }
}
